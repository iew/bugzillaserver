# README #

This project holds the configuration settings for the IEW Bugzilla Server

### Running server with Docker ###

* docker build -t iew-bugzilla .
* docker run -i -p 8082:80 iew-bugzilla

### Misc. ###
* "Couldn't connect to Docker daemon" on Mac and Cygwin --> eval "$(docker-machine env default)"

