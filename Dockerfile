# Bugzilla Server
FROM centos:7
MAINTAINER Greg Donarum <gdonarum@omi.com>

# dependencies
RUN yum -y update
RUN yum -y install wget
# add epel for redhat
RUN wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-6.noarch.rpm
RUN rpm -ivh epel-release-7-6.noarch.rpm
RUN yum -y install httpd
RUN yum -y install mariadb-server mariadb
RUN yum -y install gcc perl* mod_perl-devel

# install
ENV bugzilla_version 5.0.3
ENV bugzilla_dir /var/www/html/bugzilla
RUN wget https://ftp.mozilla.org/pub/mozilla.org/webtools/bugzilla-${bugzilla_version}.tar.gz
RUN tar xvfz bugzilla-${bugzilla_version}.tar.gz
RUN mv bugzilla-${bugzilla_version} ${bugzilla_dir}
WORKDIR ${bugzilla_dir}
COPY bugs.sql /tmp
#RUN mysql < /tmp/bugs.sql
RUN /usr/bin/perl install-module.pl --all

# run
CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]




# Notes for non Docker
# systemctl enable mariadb
# systemctl start mariadb
# mysql
# mysql> GRANT SELECT, INSERT,
#          UPDATE, DELETE, INDEX, ALTER, CREATE, LOCK TABLES,
#          CREATE TEMPORARY TABLES, DROP, REFERENCES ON bugs.*
#          TO bugs@localhost IDENTIFIED BY '$db_pass';
# mysql> FLUSH PRIVILEGES;

# grep /usr/sbin/httpd /var/log/audit/audit.log | audit2allow -M mypol
# semodule -i mypol.pp
# /sbin/restorecon -v /var/www/html/bugzilla/.htaccess
# /sbin/restorecon -v /var/www/html/bugzilla/index.cgi
# sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
# sudo firewall-cmd --reload
# apply se linux permissions
# restorecon -Rv /var/www/html 